# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased

*to be filled*

## [2.0.0] - 2019-05-26

### Added
- babel/plugin-transform-runtime.  This enables usage of async/await.

### Changed
- Updated dependencies

### BREAKING CHANGES
- Updated react-hot-loader.  Your `index.js` will need changes.

## [1.2.0] - 2018-04-14

### Added
- [CHANGELOG.md](CHANGELOG.md)

### Changed
- Updated webpack to v4

### Removed
- dotenv support

/* eslint import/no-dynamic-require:off, global-require:off */
const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

let custom;
try {
  custom = require(path.join(process.cwd(), 'webpack.dev.js'));
} catch (error) {
  custom = {};
}

module.exports = merge(common, {
  mode: 'development',
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom'
    }
  },
  entry: [
    // activate HMR for React
    'react-hot-loader/patch',

    // bundle the client for webpack-dev-server
    // and connect to the provided endpoint
    'webpack-dev-server/client?http://localhost:8080',

    // the entry point of our app
    path.resolve(process.cwd(), 'src', 'index.js')
  ],
  output: {
    // necessary for HMR to know where to load the hot update chunks
    publicPath: 'http://localhost:8080/'
  },
  devtool: 'eval',
  plugins: [
    // enable HMR globally
    new webpack.HotModuleReplacementPlugin()
  ]
}, custom);

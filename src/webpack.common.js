/* eslint import/no-dynamic-require:off, global-require:off */
const path = require('path');
const { merge } = require('webpack-merge');

let custom;
try {
  custom = require(path.join(process.cwd(), 'webpack.config.js'));
} catch (error) {
  custom = {};
}

module.exports = merge({
  entry: path.resolve(process.cwd(), 'src', 'index.js'),

  output: {
    // the output bundle
    filename: 'bundle.js',

    path: path.resolve(process.cwd(), 'public')
  },

  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react'
            ],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-transform-runtime',
              'react-hot-loader/babel'
            ]
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/[hash].[ext]'
            }
          }
        ]
      }
    ]
  }
}, custom);

const config = {
  contentBase: './public/',
  publicPath: '/',
  host: 'localhost',
  port: 8080,
  hot: true,
  historyApiFallback: true
};

if (process.env.HOST && process.env.PORT) {
  const target = `http://${process.env.HOST}:${process.env.PORT}`;
  console.log(`Proxying to ${target}`);
  config.proxy = {
    '*': target
  };
}

module.exports = config;

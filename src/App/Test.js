import React from 'react';

export default class Test extends React.Component {
  testAsync = async () => {
    console.log('Testing async/await');
    await new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
    console.log('Resolved...');
  };

  render() {
    return (
      <div>
        <h2>React Boilerplate Tests</h2>
        <button type="button" onClick={this.testAsync}>Async action</button>
      </div>
    );
  }
}

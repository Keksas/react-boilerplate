import React from 'react';
import { hot } from 'react-hot-loader';
import Test from './Test';

/**
 * App component
 * @returns {node} App component
 */
export default hot(module)(() => (
  <div>
    <h1>Hello, world!</h1>
    <pre>{JSON.stringify(process, null, 4)}</pre>
    <Test />
  </div>
));

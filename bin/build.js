#!/usr/bin/env node

const path = require('path');
const webpack = require('webpack');

const config = require(path.resolve(__dirname, '..', 'src', 'webpack.production.js'));
const compiler = webpack(config);
compiler.run((err) => {
    if (err) {
        throw err;
    }
    console.log('Done');
});

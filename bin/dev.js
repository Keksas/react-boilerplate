#!/usr/bin/env node

const path = require('path');
const Server = require('webpack-dev-server');
const webpack = require('webpack');

const config = require(path.resolve(__dirname, '..', 'src', 'webpack.dev.js'));
const devConfig = require(path.resolve(__dirname, '..', 'src', 'dev-server.js'));
const compiler = webpack(config);

const server = new Server(compiler, devConfig);

server.listen(devConfig.port, devConfig.host, () => {
    console.log(`webpack-dev-server: http://${devConfig.host}:${devConfig.port}`);
    console.log('webpack: Compiling...');
});

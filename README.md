React boilerplate that includes react, hot-reloading, sass and redux support.

An example can be found [here](https://gitlab.com/Keksas/react-boilerplate-example)
